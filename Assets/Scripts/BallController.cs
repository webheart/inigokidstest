﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    [SerializeField] [Range(10, 1000)] private float _startForce;
    [SerializeField] [Range(0, 180)] private float _startAngle;
    [SerializeField] private bool _randomStartAngle;
    [SerializeField] private SpriteRenderer _renderer;

    [SerializeField] [Range(1, 4)] private int _ballPower;

    public int BallPower
    {
        get { return _ballPower; }
        set
        {
            _ballPower = value;
            ChangeColor();
        }
    }

    // Use this for initialization
    void Start()
    {
        ChangeColor();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void IncreasePower(int seconds, int power)
    {
        BallPower = Mathf.Clamp(BallPower + power, 1, 4);
        StartCoroutine("IncreasePowerCoroutine", seconds);
    }

    IEnumerator IncreasePowerCoroutine(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        BallPower = 1;
    }

    void ChangeColor()
    {
        switch (BallPower)
        {
            case 1:
                _renderer.color = Color.white;
                break;
            case 2:
                _renderer.color = Color.yellow;
                break;
            case 3:
                _renderer.color = Color.red;
                break;
            case 4:
                _renderer.color = Color.black;
                break;
            default:
                _renderer.color = Color.white;
                break;
        }
    }

    public void StartPlaying()
    {
        if (_randomStartAngle)
        {
            _startAngle = Random.Range(30, 160);
        }
        var startX = -_startForce * Mathf.Cos(_startAngle / 180 * Mathf.PI);
        var startY = _startForce * Mathf.Sin(_startAngle / 180 * Mathf.PI);
        GetComponent<Rigidbody2D>().AddForce(new Vector2(startX, startY));
    }
}