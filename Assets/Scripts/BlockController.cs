﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour
{
    private int _blockLives = 1;

    [SerializeField] private SpriteRenderer _renderer;
    [SerializeField]
    [Range(1,100)]private int _boostChance = 5;
    // Use this for initialization
    void Start()
    {
        _blockLives = Random.Range(1, 4);
        ChangeColor();
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnCollisionExit2D(Collision2D collider)
    {
        if (collider.gameObject.GetComponent<BallController>())
        {
            var ball = collider.gameObject.GetComponent<BallController>();
            TakeHit(ball.BallPower);
            var rand = Random.Range(1, 100);
            if (rand < _boostChance)
            {
                ball.IncreasePower(10, Random.Range(1, 4));
            }
        }
    }

    void TakeHit(int modifier)
    {
        _blockLives -= modifier;
        if (_blockLives <= 0)
            BlockManager.Instance.OnDestroyBlock(this);
        ChangeColor();
    }

    void ChangeColor()
    {
        switch (_blockLives)
        {
            case 1:
                _renderer.color = Color.cyan;
                break;
            case 2:
                _renderer.color = Color.blue;
                break;
            case 3:
                _renderer.color = Color.magenta;
                break;
            case 4:
                _renderer.color = Color.red;
                break;
            default:
                _renderer.color = Color.yellow;
                break;
        }
    }
}