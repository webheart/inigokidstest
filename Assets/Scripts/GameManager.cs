﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    [SerializeField] private GameObject _ballPrefab;
    [SerializeField] private GameObject _bitPrefab;
    [SerializeField] [Range(0, 100)] private int _livesCount = 3;
    [SerializeField] private float _bitPosX;
    [SerializeField] private float _bitPosY;
    [SerializeField] private Transform _gameOverWindow;
    [SerializeField] private Text _GameOverText;
    [SerializeField] private Text _livesCountText;
    [SerializeField] private Text _scoreText;

    private int _startedLivesCount;
    private int _score = 0;
    private BitController _bitController;

    void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        _gameOverWindow.gameObject.SetActive(false);
        _startedLivesCount = _livesCount;
        _bitController = SpawnBit();
        _bitController.Ball = SpawnBall();
        _scoreText.text = string.Format("Score: {0}", _score);
    }

    // Update is called once per frame
    void Update()
    {
    }

    private BallController SpawnBall()
    {
        ChangeLivesCount(-1);
        return Instantiate(_ballPrefab, new Vector3(_bitPosX, _bitPosY + 1), Quaternion.identity)
            .GetComponent<BallController>();
    }

    private BitController SpawnBit()
    {
        return Instantiate(_bitPrefab, new Vector3(_bitPosX, _bitPosY), Quaternion.identity)
            .GetComponent<BitController>();
    }

    public void ChangeLivesCount(int modifier)
    {
        _livesCount += modifier;
        _livesCountText.text = string.Format("Balls remaining: {0}", Mathf.Clamp(_livesCount, 0, Int32.MaxValue));
    }

    public void ChangeScore(int modifier)
    {
        _score+= modifier;
        _scoreText.text = string.Format("Score: {0}", _score);
    }

    public void OnLoseBall()
    {
        _bitController.StopGame();
        Destroy(_bitController.Ball.gameObject);
        if (_livesCount > 0)
        {
            _bitController.Ball = SpawnBall();
        }
        else
        {
            _gameOverWindow.gameObject.SetActive(true);
            _GameOverText.text = string.Format("Defeat! Your score: {0}", _score);
        }
    }

    public void OnDestroyedAllBlocks()
    {
        _bitController.StopGame();
        Destroy(_bitController.Ball.gameObject);
        _gameOverWindow.gameObject.SetActive(true);
        _GameOverText.text = string.Format("You Win! Your score: {0}", _score);
    }

    public void ResetGame()
    {
        _score = 0;
        _livesCount = _startedLivesCount;
        _bitController.Ball = SpawnBall();
        BlockManager.Instance.ResetBlocks();
        _gameOverWindow.gameObject.SetActive(false);
    }
}

public enum GameOverType
{
    Win,
    Loose
}