﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockManager : MonoBehaviour
{
    public static BlockManager Instance;

    [SerializeField] private int _blocksInRowCount;
    [SerializeField] private int _rowCount;
    [SerializeField] private float _blocksPositionY;
    [SerializeField] private float _gapX = 0;
    [SerializeField] private float _gapY = 0;
    [SerializeField] private GameObject _blockPrefab;

    private List<GameObject> _blockList = new List<GameObject>();

    void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        GetBlocks();
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void GetBlocks()
    {
        var rowPosition = _blocksPositionY;
        for (int i = 0; i < _rowCount; i++)
        {
            for (int j = 0; j < _blocksInRowCount; j++)
            {
                var xPos = ((-_blocksInRowCount + 1) / 2f + j) * (_gapX + 1);
                var block = Instantiate(_blockPrefab, new Vector3(xPos, rowPosition), Quaternion.identity, transform);
                _blockList.Add(block);
            }
            rowPosition -= (1 + _gapY);
        }
    }

    public void OnDestroyBlock(BlockController block)
    {
        if (_blockList.Contains(block.gameObject))
            _blockList.Remove(block.gameObject);
        Destroy(block.gameObject);
        GameManager.Instance.ChangeScore(1);
        if (_blockList.Count == 0)
        {
            GameManager.Instance.OnDestroyedAllBlocks();
        }
    }

    public void ResetBlocks()
    {
        foreach (var o in _blockList.ToArray())
        {
            Destroy(o);
        }
        _blockList.Clear();
        GetBlocks();
    }
}