﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BitController : MonoBehaviour
{
    [SerializeField][Range(0.01f, 1)] private float _speed = 0.1f;

    [NonSerialized] public BallController Ball;

    private bool _isStarted = false;

    private float _leftBorderPos;
    private float _rightBorderPos;
    // Use this for initialization
    void Start()
    {
        GetBorders();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            MoveLeft();
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            MoveRight();
        }
        if (Input.GetKeyDown(KeyCode.Space) && !_isStarted)
        {
            StartGame();
        }
        if (!_isStarted && Ball != null)
        {
            Ball.gameObject.transform.position = transform.position + new Vector3(0, 1);
        }
    }

    void GetBorders()
    {
        var leftTransform = GameObject.Find("LeftBorder").transform;
        _leftBorderPos = leftTransform.position.x + leftTransform.lossyScale.x / 2;
        var rightTransform = GameObject.Find("RightBorder").transform;
        _rightBorderPos= rightTransform.position.x - leftTransform.lossyScale.x / 2;
    }
    void MoveLeft()
    {
        bool isBorder = transform.position.x - transform.lossyScale.x / 2 <= _leftBorderPos;
        if(isBorder) return;
        gameObject.transform.position -= new Vector3(_speed, 0);
    }
    void MoveRight()
    {
        bool isBorder = transform.position.x + transform.lossyScale.x / 2 >= _rightBorderPos;
        if (isBorder) return;
        gameObject.transform.position += new Vector3(_speed, 0);
    }

    public void StartGame()
    {
        Ball.StartPlaying();
        _isStarted = true;
    }

    public void StopGame()
    {
        _isStarted = false;
    }
}