﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomBorderController : MonoBehaviour {

    // Use this for initialization
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<BallController>())
        {
            GameManager.Instance.OnLoseBall();
        }
    }
}
